/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef MessageViewController_h
#define MessageViewController_h

@class NSTextView,NSScrollView,NSMutableDictionary;
@class CWMessage;

#include "MsgDB.h"

@interface MessageViewController : NSObject
{
	NSTextView *tv;
	NSScrollView *sv;

	MsgDB *mdb;
	msg_id_t mid;
	CWMessage *cur_message;

	/* actually an NSMapTable */
	void *cur_options;

	int cur_font;
	int show_source;
}

- initWithMsgDB: (MsgDB *)m  textView: (NSTextView *)textview  scrollView: (NSScrollView *)scrollview;

-(void) setMid: (msg_id_t)m;

-(BOOL) atEnd;

/* returns YES if we hit the end of the message */
-(BOOL) scrollDown;

-(void) lineUp;
-(void) lineDown;


-(void) switchFont;

-(void) showSource;


-(void) messageDownload;

-(void) messageSave;

-(void) encodingChanged: (NSNotification *)n;

@end

#endif

