/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef MsgDB_h
#define MsgDB_h


typedef unsigned int msg_id_t;




@class NSMutableDictionary,NSNotificationCenter,NSString;


#define DSTATUS_NODATA   0
#define DSTATUS_DATA     1
#define DSTATUS_PENDING  2
#define DSTATUS_ERROR    3
#define DSTATUS_NOSOURCE 4


@class MsgDB;

@protocol Msg_Source <NSObject>
- initWithMsg: (msg_id_t)mid db: (MsgDB *)mdb;
-(msg_id_t) mid;
-(void) update;
-(void) disconnect;

-(unsigned int) getMessage: (msg_id_t)mid  priority: (int)pri;
-(BOOL) cancelGetMessage: (msg_id_t)mid  id: (unsigned int) d;
@end


@protocol Msg_Folder <NSObject>
-(int) numMessages;
-(msg_id_t *) getMessages;
@end


@class NSString;
@class NSMutableArray;

@interface MsgDB : NSObject
{
	msg_id_t main_id;

	NSString *dir;

	NSMutableDictionary *sources;
	msg_id_t last_source_id;

	struct message_s *messages;
	unsigned int num_messages;

	char **meta_headers;
	int num_meta_headers;

	struct msgdb_hash_s *hash;

	NSMutableDictionary *folders;

	NSNotificationCenter *ncenter;
}

- initWithDirectory: (NSString *)adir;


-(NSNotificationCenter *)notificationCenter;
-(void)setNotificationCenter: (NSNotificationCenter *)nc;


-(msg_id_t) midForId: (const char *)msgid;

-(msg_id_t) createMessageWithId: (const char *)msgid  source: (NSObject<Msg_Source> *)src;
-(void) setMessageData: (unsigned char *)d length: (int)l : (msg_id_t) mid;

-(void) msg_setSource: (NSObject<Msg_Source> *)src : (msg_id_t) mid;

-(void) updateAll;


-(const char *) msg_getMessageID: (msg_id_t)mid;

-(const char *)msg_getMetaHeader: (const char *)hname : (msg_id_t)mid;
-(void)msg_setMetaHeader: (const char *)hname  value: (const char *)value : (msg_id_t)mid;

-(const char *)msg_getMetaHeaderWithNumber: (int)num : (msg_id_t)mid;
-(void)msg_setMetaHeaderWithNumber: (int)num  value: (const char *)value : (msg_id_t)mid;

-(const char *)msg_getHeader: (const char *)hname : (msg_id_t)mid;

/* priority ~10 for something the user wants to view, ~0 for read-ahead,
~-10 for bulk download */
-(void) msg_wantData: (msg_id_t)mid  priority: (int)pri;
-(void) msg_needData: (msg_id_t)mid;
-(void) msg_cancelWantData: (msg_id_t)mid;


-(int) msg_getData: (const unsigned char **)data length: (int *)length : (msg_id_t)mid;
-(int) msg_dstatus: (msg_id_t)mid;


-(void)msg_addToFolder: (const char *)foldername : (msg_id_t)mid;



-(int) getMetaHeaderNum: (const char *)hname;
-(const char *) getMetaHeaderName: (int)num;


-(void) syncToDisk;


-(void)dumpMessages;
-(void)dumpMessage: (msg_id_t)mid;

-(NSDictionary *) folders;
-(NSArray *) sources;

-(NSObject<Msg_Source> *) addSourceOfType: (Class)c;
-(void) removeSource: (NSObject<Msg_Source> *)src;

@end



extern NSString
	*MsgDB_MsgDStatusNotification,
	*MsgDB_MsgMetaChangeNotification,

	*MsgDB_LogMessageNotification,

	*MsgDB_FolderAddMsgNotification,
	*MsgDB_FolderAddNotification,

	*MsgDB_SourceAddNotification,
	*MsgDB_SourceRemoveNotification,
	*MsgDB_SourceChangeNotification;


#include <Foundation/NSNotification.h>

@interface MidNotification : NSNotification
{
	NSString *name;
	id object;

	msg_id_t mid;
	NSObject<Msg_Folder> *folder;
	int header;
}
+ notificationWithName: (NSString *)name  object: (id)o mid: (msg_id_t)m  folder: (NSObject<Msg_Folder> *)f  header: (int)h;
- initWithName: (NSString *)n  object: (id)o  mid: (msg_id_t)m  folder: (NSObject<Msg_Folder> *)f  header: (int)h;
-(msg_id_t) mid;
-(NSObject<Msg_Folder> *) folder;
-(const char *) headerName;
@end


#endif

