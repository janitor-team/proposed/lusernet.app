/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#include <Foundation/NSObject.h>
#include <Foundation/NSBundle.h>
#include <AppKit/NSWindow.h>
#include <AppKit/NSScrollView.h>
#include <AppKit/NSTextView.h>
#include <AppKit/NSTextContainer.h>
#include <AppKit/NSTextStorage.h>
#include <AppKit/NSClipView.h>

#include "LogWindowController.h"


@implementation LogWindowController

- init
{
	NSWindow *win;

	win=[[NSWindow alloc] initWithContentRect: NSMakeRect(100,100,500,200)
		styleMask: NSClosableWindowMask|NSTitledWindowMask|NSResizableWindowMask|NSMiniaturizableWindowMask
		backing: NSBackingStoreRetained
		defer: YES];
	if (!(self=[super initWithWindow: win])) return nil;

	{ /* TODO: fix properly */
		NSScrollView *sv;
//		NSRect r;

//		r=NSMakeRect(0,0,500,200);

		[win setTitle: _(@"Log")];

//		sv=[[NSScrollView alloc] initWithFrame: r];
		sv=[[NSScrollView alloc] init];
//		[sv setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
//		[sv setAutoresizesSubviews: YES];
		[sv setHasVerticalScroller: YES];
		[sv setHasHorizontalScroller: NO];
		[sv setBorderType: NSBezelBorder];

		log=[[NSTextView alloc] initWithFrame: [[sv contentView] frame]];
//		log=[[NSTextView alloc] init];
		[log setHorizontallyResizable: NO];
		[log setVerticallyResizable: YES];
		[log setEditable: NO];
		[[log textContainer] setWidthTracksTextView: YES];
		[[log textContainer] setHeightTracksTextView: NO];
		[[log textContainer] setContainerSize: NSMakeSize(0,1e6)];
		[log setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];

		[sv setDocumentView: log];

		[win setContentView: sv];

		[sv release];
	}

	[win setFrameUsingName: @"LogWindow"];
	[win setFrameAutosaveName: @"LogWindow"];

	[win release];

	return self;
}

-(void) dealloc
{
	DESTROY(log);
	[super dealloc];
}


-(void) addLogString: (NSString *)s
{
	NSTextStorage *ts;
	int l;

	ts=[log textStorage];
	[ts beginEditing];
	l=[ts length];
	[ts replaceCharactersInRange: NSMakeRange(l,0) withString: s];
	l+=[s length];
	[ts replaceCharactersInRange: NSMakeRange(l,0) withString: @"\n"];
	[ts endEditing];
	[log scrollRangeToVisible: NSMakeRange(l-1,1)];
}

@end

