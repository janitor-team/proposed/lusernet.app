This is (supposed to be) a list of user-visible changes between versions.


-- changes in version 0.4.1 - 2002-04-07 23:50

* Added posting support! Currently quite basic, but useable. The Compose
 menu will let you write a new article or reply to an existing. There's
 a preferences 'tab' that lets you set the default name and address,
 and the source of the default signature.

  The compose windows lets you change write the article and change the
 subject and from-address, as well as the list of groups the article
 should be posted to. If you want to crosspost (note that this is often
 considered bad style), write the group names separated by commas and
 nothing else (no whitespace). When you click post the article will be
 sent, and if it was sent succesfully the window will close. If you want
 to save a copy of the article locally, you'll need to copy and paste the
 text out of the window manually for now.

  If an article could not be sent, the log window will probably have an
 explanation. Note that GNUstep's NSTextView is quite slow, so when
 responding to a long article it might take a long while for the compose
 window to pop up.

  Note that correct format=flowed handling requires a future version of
 Pantomime, so the default posting method should be 'quoted-printable'
 (unless you are _really_ sure that you know what you're doing).

* New key equivalents, 'alt-s' to save the current message and 'alt-F' to
 switch font.

* Basic support for decoding uuencoded parts of messages. Note that this
 probably requires a future Pantomime version.

* There's optional support for MIME handling (ie. inline display of images,
 html, rtf, etc.) using an not-yet released MIME handling library. If you're
 brave and want to test it, contact me.

* The French and Spanish translations are still not up-to-date (and it's
 still my fault).


-- changes in version 0.4.0 - 2002-03-25 01:15

* Added a French translation by St�phane Peron. Thanks!

* Added more read-ahead options to the preferences panel.

* Updated the documentation.

* Cleanups and bug fixes. Note that the French and Spanish translations are
 not currently up-to-date (which is mostly my fault).


-- changes in version 0.3.9.4 (yet another test release) - 2002-03-11 02:50

* Added an Edit menu with a Copy entry.


-- changes in version 0.3.9.3 (yet another test release) - 2002-03-11 02:30

* Implemented a real preferences panel. Info->Preferences will bring it up.
 There are currently 'tabs' for sources (basically the old preferences
 panel), message viewing, and read-ahead. (Almost) all options can be
 changed there, so there's no longer any need to mess around with default
 keys. Hopefully the descriptions there will be enough to understand the
 options. (If not, tell me what isn't clear and I'll do something about
 it.)

* Fixed bugs.


-- changes in version 0.3.9.2 (yet another test release) - 2002-03-09 18:30

* Added a size limit for automatic downloading of messages. The defaults key
 is 'AutoDownloadSizeLimit', and the default is 64kb. Messages larger than
 this won't be downloaded automatically when you select them, but you can
 still select download in the menu or click on the link in the message
 if you want to download them. The size of the message will be displayed
 (if known).

* Fixed bug.


-- changes in version 0.3.9.1 (another test release) - 2002-03-09 15:00

* Intelligent scrolling. When you hit ' ' to scroll down, quoted sections,
 blank lines and signatures will be automatically skipped. (This is
 optional (just set the defaults key "IntelligentScroll" to NO), but I
 find it very useful.) It should provide context for the part it skips to,
 but if it isn't enough you can use 's' and 'x' to get more.

* Message rendering has been improved a lot.	Headers and informative text
 from LuserNET is written in dark blue. Red text among headers are
 clickable. Normal message text is usually black, thought quoted parts
 might be colored differently (the colors have also been adjusted to
 look better).

 Data in attachments can saved by clicking 'Save'. In 'multipart/alternative'
 messages, you can click on the part you want to view (so you can view the
 text/plain instead of text/html). text/* parts that couldn't be decoded
 can be viewed as ascii text can be viewed as ASCII text (the ASCII text
 might not match what the sender intended, but it'll often be close).

* Intelligent read-ahead. When it is active (defaults key 'ReadAhead',
 defaults is YES), LuserNET will start background downloads of the next,
 previous, next unread, and next-unread-past-this-thread messages. This
 means that by the time you've finished reading, hitting 'n' or ' ' will
 bring the next message up instantly.

 The defaults key 'ReadAheadSizeLimit' has the maximum size of a message
 that will be downloaded as read-ahead. The defaults value is 20kb.

* There's a menu entry for showing the raw source for a message.

* New headers will be parsed incrementally, instead of waiting until all
 have been downloaded and parsing them in one go.

* Removed console version code from the released version. Still available
 by request, if anyone's interested.

* Bug fixes. Optimizations. Cleanups. The usual.


-- changes in version 0.3.9 (test release) - 2002-03-04 02:50

* If a thread is really deep the thread depth gets display as a number.

* Headers will be parsed from the message's data if we didn't get them
 with xover.

* If a message doesn't have a source the 'no source'-message will let you
 select one.

* Key navigation should work in most dialogs.

* Updated for new Pantomime changes. (Won't work with old versions.)

* Bug fixes. Meta header database is slightly incompatible at the moment.



-- changes in version 0.3.4 - 2002-02-26 13:00

* 's' and 'x' move up and down a small amount in the current message.

* Added a spanish translation contributed by Quique. Thanks!

* Message->Download will download a message. Usually messages are
 downloaded automatically, but if there was an error you can use this
 to force a new download attempt (useful when messages have arrived out
 of order).

* 'Message->Switch font' will switch between the primary and secondary
 font. The default keys MessageFont1 and MessageFont1Size specify the
 primary font, and MessageFont2/... the secondary font. By default the
 primary font is the normal user font and the secondary font is the
 normal user fixed-pitch font. No GUI to change this yet...

* The defaults key ColorMessages controls whether messages are colored
 or not. Default is to color, you can change it by running:
   defaults write LuserNET ColorMessages NO
 (or YES). No GUI for this yet...

* Removed yellow and green from the list of colors to use when coloring
 messages since they were difficult to read on the white background.

* Fix some silly typos in the localization files. Start using make_strings
 to keep them up-to-date.

* Fixed a bug that caused a 'FolderSortMode_(nil)' key to appear in the
 defaults (it causes no harm, but it doesn't do anything and shouldn't be
 there).

* Disabled double-release checking. This increases performance a fair
 amount (especially when parsing and coloring large messages).


-- changes in version 0.3.3 - 2002-02-23 23:50

* Messages can be saved from the menu. The raw data will be saved, including
 headers.

* The date for messages is now parsed correctly and will be displayed
 intelligently in the folder window.

* Messages can be arranged by thread, reverse-thread (newer threads above
 older threads), arrival order and reverse arrival order. Thread-based
 movement commands only work in normal thread mode currently.

* The command line configuration stuff is gone.

* The menus have been rearranged.

* 'P' will move to a message's parent.

* The program will remember which windows/folders you had open when you
 exited the program and will open them again when you start it.

* Lots of minor fixed, cleanups and optimizations.



-- changes in version 0.3.2 - 2002-02-19 23:07

* Fix bug in handling 'A' in folder windows (it'd work but it'd beep anyway).

* Added localization support and a swedish translation.

* Menus have been rearranged and extended.

* Table column positions will be saved.



-- version 0.3.1 - 2002-02-19 01:54

