/*
 *
 * Copyright 2002 Alexander Malmberg <alexander@malmberg.org>
 *           2012 The Free Software Foundation
 *
 * Authors: Alexander Malmberg <alexander@malmberg.org>
 *          Riccardo Mottola <rm@gnu.org>
 *
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSBundle.h>
#import <Foundation/NSDictionary.h>
#import <AppKit/NSWindow.h>
#import <AppKit/NSScrollView.h>
#import <AppKit/NSTableView.h>
#import <AppKit/NSTableColumn.h>
#import <AppKit/NSCell.h>

#import "FolderListController.h"

#import "MsgDB.h"
#import "main.h"


@implementation FolderListController

-(void) updateFolderNames
{
	NSEnumerator *e;
	NSString *s;
	DESTROY(folder_names);
	folder_names=[[NSMutableArray alloc] init];
	for (e=[[mdb folders] keyEnumerator];(s=[e nextObject]);)
		[folder_names addObject: [s copy]];
	[folder_names sortUsingSelector: @selector(compare:)];

	[folders reloadData];
}

-(int) numberOfRowsInTableView: (NSTableView *)tv
{
	return [folder_names count];
}

-(id) tableView: (NSTableView *)tv  objectValueForTableColumn: (NSTableColumn *)tc  row: (int)row
{
	if (tc==c_name)
		return [folder_names objectAtIndex: row];
	else
	if (tc==c_num)
		return [NSNumber numberWithInt: [[[mdb folders] objectForKey: [folder_names objectAtIndex: row]] numMessages]];
	else
		abort();
}


- initWithMsgDB: (MsgDB *)m;
{
	NSWindow *win;
	win=[[NSWindow alloc] initWithContentRect: NSMakeRect(100,100,250,200)
		styleMask: NSClosableWindowMask|NSTitledWindowMask|NSResizableWindowMask|NSMiniaturizableWindowMask
		backing: NSBackingStoreRetained
		defer: YES];

	if (!(self=[super initWithWindow: win])) return nil;

	ASSIGN(mdb,m);

	folder_names=[[NSMutableArray alloc] init];

	{
		NSScrollView *sv;

		[win setTitle: _(@"Folder list")];

		c_name=[[NSTableColumn alloc] initWithIdentifier: @"Name"];
		[[c_name headerCell] setStringValue: _(@"Name")];
		[c_name setEditable: NO];
		[c_name setResizable: YES];
		[c_name setWidth: 180];

		c_num=[[NSTableColumn alloc] initWithIdentifier: @"Messages"];
		[[c_num headerCell] setStringValue: _(@"Messages")];
		[[c_num headerCell] setAlignment: NSRightTextAlignment];
		[[c_num dataCell] setAlignment: NSRightTextAlignment];
		[c_num setEditable: NO];
		[c_num setResizable: YES];
		[c_num setWidth: 50];

		folders=[[NSTableView alloc] init];
		[folders setAllowsColumnReordering: YES];
		[folders setAllowsColumnResizing: YES];
		[folders setAllowsMultipleSelection: NO];
		[folders setAllowsColumnSelection: NO];
		[folders addTableColumn: c_name];
		[folders addTableColumn: c_num];
		[folders setDataSource: self];
		[folders setDoubleAction: @selector(openFolder:)];

		[folders setAutosaveName: @"FolderList"];
		[folders setAutosaveTableColumns: YES];

		sv=[[NSScrollView alloc] init];
		[sv setDocumentView: folders];
		[sv setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
		[sv setAutoresizesSubviews: YES];
		[sv setHasVerticalScroller: YES];
		[sv setBorderType: NSBezelBorder];

		[win setContentView: sv];

		[sv release];
	}
	[win setDelegate: self];
	[win setFrameUsingName: @"FolderList"];
	[win setFrameAutosaveName: @"FolderList"];

	[win release];

	[self updateFolderNames];

	[[NSNotificationCenter defaultCenter]
		addObserver: self
		selector: @selector(updateFolderNames)
		name: MsgDB_FolderAddMsgNotification
		object: mdb];

	[[NSNotificationCenter defaultCenter]
		addObserver: self
		selector: @selector(updateFolderNames)
		name: MsgDB_FolderAddNotification
		object: mdb];

	return self;
}

-(void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver: self];

	DESTROY(c_name);
	DESTROY(c_num);
	DESTROY(folders);

	DESTROY(folder_names);
	DESTROY(mdb);
	[super dealloc];
}


-(void) display
{
	[[self window] makeKeyAndOrderFront: self];
}

-(void) openFolder: (id)sender
{
	int r=[folders selectedRow];
	if (r>=0 && r<[folder_names count])
		[app_delegate openFolderWindow: [folder_names objectAtIndex: r]];
}

@end

