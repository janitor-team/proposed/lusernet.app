/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef LogWindowController_h
#define LogWindowController_h

#include <AppKit/NSWindowController.h>

@class NSTextView;

@interface LogWindowController : NSWindowController
{
	NSTextView *log;
}

- init;

-(void) addLogString: (NSString *)s;

@end

#endif

