/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#include <Foundation/NSObject.h>
#include <Foundation/NSString.h>
#include <Foundation/NSUserDefaults.h>
#include <Foundation/NSBundle.h>
#include <AppKit/NSButton.h>
#include <AppKit/NSTextField.h>
#include <GNUstepGUI/GSVbox.h>
#include <GNUstepGUI/GSHbox.h>
#include <AppKit/NSBox.h>

#include "Pref_ReadAhead.h"


#define ReadAheadKey @"ReadAhead"
#define ReadAheadSizeLimitKey @"ReadAheadSizeLimit"

#define ReadAheadNextKey @"ReadAheadNext"
#define ReadAheadPreviousKey @"ReadAheadPrevious"
#define ReadAheadNextThreadKey @"ReadAheadNextThread"
#define ReadAheadNextUnreadKey @"ReadAheadNextUnread"

static NSUserDefaults *sd;


@implementation Pref_ReadAhead

+(void) initialize
{
	if (!sd)
		sd=[NSUserDefaults standardUserDefaults];
}

+(int) readAheadSizeLimit
{
	if ([sd objectForKey: ReadAheadSizeLimitKey])
		return [sd integerForKey: ReadAheadSizeLimitKey];
	else
		return 16*1024;
}

/*
The underscores in the names are ugly, but gcc 2.95.3 doesn't accept
eg. 'RA_GET_BOOL()'.
*/
#define readAhead_ readAhead
#define ReadAhead_Key ReadAheadKey
#define b_ReadAhead_ b_ReadAhead

#define RA_GET_BOOL(x) \
	+(BOOL) readAhead##x \
	{ \
		if (![sd objectForKey: ReadAhead##x##Key] || \
		    [sd boolForKey: ReadAhead##x##Key]) \
			return YES; \
		else \
			return NO; \
	}

RA_GET_BOOL(_)

RA_GET_BOOL(NextUnread)
RA_GET_BOOL(NextThread)
RA_GET_BOOL(Next)
RA_GET_BOOL(Previous)

#undef RA_GET_BOOL

-(void) save
{
	NSUserDefaults *sd=[NSUserDefaults standardUserDefaults];
	BOOL b;

	if (!top) return;

#define SAVE_BOOL(x) \
	b=[b_ReadAhead##x state]?YES:NO; \
	[sd setBool: b forKey: ReadAhead##x##Key];

	SAVE_BOOL(_)

	SAVE_BOOL(NextUnread)
	SAVE_BOOL(NextThread)
	SAVE_BOOL(Next)
	SAVE_BOOL(Previous)

#undef SAVE_BOOL

	[sd setInteger: [f_ReadAheadSizeLimit intValue] forKey: ReadAheadSizeLimitKey];
}

-(void) revert
{
#define REVERT_BOOL(x) \
	if ([Pref_ReadAhead readAhead##x]) \
		[b_ReadAhead##x setState: 1]; \
	else \
		[b_ReadAhead##x setState: 0];

	REVERT_BOOL(_)

	REVERT_BOOL(NextUnread)
	REVERT_BOOL(NextThread)
	REVERT_BOOL(Next)
	REVERT_BOOL(Previous)

#undef REVERT_BOOL

	[f_ReadAheadSizeLimit setIntValue: [Pref_ReadAhead readAheadSizeLimit]];
}


-(NSString *) name
{
	return _(@"Read-ahead");
}

-(void) setupButton: (NSButton *)b
{
	[b setTitle: _(@"Read-\nahead")];
	[b sizeToFit];
}

-(void) willHide
{
}

-(NSView *) willShow
{
	if (!top)
	{
		top=[[GSVbox alloc] init];
		[top setDefaultMinYMargin: 4];

		{
			NSTextField *f;
			GSHbox *hb;

			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Read-ahead downloads messages smaller than:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_ReadAheadSizeLimit=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable];
			[f sizeToFit];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);
		}

		{
			NSButton *b;

			NSBox *box;
			GSVbox *vb;

			box=[[NSBox alloc] init];
			[box setTitle: @"Read ahead what?"];

			vb=[[GSVbox alloc] init];
			[vb setDefaultMinYMargin: 4];

			b_ReadAheadNextThread=b=[[NSButton alloc] init];
			[b setTitle: _(@"Next unread past the current thread.")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[vb addView: b enablingYResizing: NO];
			DESTROY(b);

			b_ReadAheadNextUnread=b=[[NSButton alloc] init];
			[b setTitle: _(@"Next unread.")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[vb addView: b enablingYResizing: NO];
			DESTROY(b);

			b_ReadAheadPrevious=b=[[NSButton alloc] init];
			[b setTitle: _(@"Previous.")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[vb addView: b enablingYResizing: NO];
			DESTROY(b);

			b_ReadAheadNext=b=[[NSButton alloc] init];
			[b setTitle: _(@"Next.")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[vb addView: b enablingYResizing: NO];
			DESTROY(b);

			[vb sizeToFit];
			[box setContentView: vb];
			[box sizeToFit];
			DESTROY(vb);
			[top addView: box enablingYResizing: NO];
			DESTROY(box);

			b_ReadAhead_=b=[[NSButton alloc] init];
			[b setTitle: _(@"Read ahead (ie. start downloading related messages\nwhen a message is selected).")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[top addView: b enablingYResizing: NO];
			DESTROY(b);
		}

		[self revert];
	}
	return top;
}

-(void) dealloc
{
	DESTROY(top);
	[super dealloc];
}

@end

