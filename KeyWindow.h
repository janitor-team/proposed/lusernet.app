/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef KeyWindow_h
#define KeyWindow_h

/*
window that passes key-events to its delegate before trying to handle them
*/

#include <AppKit/NSWindow.h>

@protocol KeyWindowDelegate
-(int) keyDown: (NSEvent *)e  inWindow: (NSWindow *)w;
@end

@interface KeyWindow : NSWindow
@end

#endif

