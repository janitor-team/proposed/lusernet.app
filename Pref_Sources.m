/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#include <Foundation/NSObject.h>
#include <Foundation/NSString.h>
#include <Foundation/NSValue.h>
#include <Foundation/NSBundle.h>

#include <AppKit/NSTableView.h>
#include <AppKit/NSTableColumn.h>
#include <AppKit/NSButton.h>
#include <AppKit/NSScrollView.h>
#include <AppKit/NSClipView.h>
#include <GNUstepGUI/GSHbox.h>
#include <GNUstepGUI/GSVbox.h>

#include "PrefBox.h"
#include "Pref_Sources.h"

#include "MsgDB.h"

#include "GUISource.h"
#include "NNTPSource.h"


@implementation Pref_Sources

-(void) addSource: (id)sender
{ /* TODO: support different kinds of sources. need at least two kinds first */
	[mdb addSourceOfType: [NNTPSource class]];
}

-(void) removeSource: (id)sender
{ /* TODO: confirm with user first? */
	if ([source_list selectedRow]!=-1)
		[mdb removeSource: [sources objectAtIndex: [source_list selectedRow]]];
}

-(void) sourceProperties: (id)sender
{
	if ([source_list selectedRow]!=-1)
		[[sources objectAtIndex: [source_list selectedRow]] displayPropertiesWindow];
}


-(int) numberOfRowsInTableView: (NSTableView *)tv
{
	return [sources count];
}

-(id) tableView: (NSTableView *)tv  objectValueForTableColumn: (NSTableColumn *)tc  row: (int)row
{
	id<Msg_Source,GUISource> s;
	s=[sources objectAtIndex: row];

	if (tc==c_num)
	{
		return [NSNumber numberWithInt: row];
	}
	else if (tc==c_name)
	{
		return [s sourceName];
	}
	else if (tc==c_type)
	{
		return [s sourceType];
	}
	else
		return @"Unknown column";
}


-(void) _getSources
{
	DESTROY(sources);
	sources=[[mdb sources] retain];
	[source_list reloadData];
}


-(void) _createViews
{
	GSHbox *hbox;

	NSScrollView *sv;

	hbox=[[GSHbox alloc] init];
	[hbox setDefaultMinXMargin: 4];

	sv=[[NSScrollView alloc] init];
	[sv setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
	[sv setHasVerticalScroller: YES];
	[sv setHasHorizontalScroller: NO];
	[sv setBorderType: NSBezelBorder];
	
	c_num=[[NSTableColumn alloc] initWithIdentifier: @"Id"];
	[[c_num headerCell] setStringValue: _(@"Id")];
	[c_num setEditable: NO];
	[c_num setResizable: YES];
	[c_num setWidth: 32];
	
	c_name=[[NSTableColumn alloc] initWithIdentifier: @"Name"];
	[[c_name headerCell] setStringValue: _(@"Name")];
	[c_name setEditable: NO];
	[c_name setResizable: YES];
	[c_name setWidth: 128];
	
	c_type=[[NSTableColumn alloc] initWithIdentifier: @"Type"];
	[[c_type headerCell] setStringValue: _(@"Type")];
	[c_type setEditable: NO];
	[c_type setResizable: YES];
	[c_type setWidth: 64];
	
	source_list=[[NSTableView alloc] initWithFrame: [[sv contentView] frame]];
	[source_list setAllowsColumnReordering: YES];
	[source_list setAllowsColumnResizing: YES];
	[source_list setAllowsMultipleSelection: NO];
	[source_list setAllowsColumnSelection: NO];
	[source_list addTableColumn: c_num];
	[source_list addTableColumn: c_name];
	[source_list addTableColumn: c_type];
	[source_list setDataSource: self];
	[source_list setDelegate: self];
	[source_list setDoubleAction: @selector(sourceProperties:)];
	
	[sv setDocumentView: source_list];
	[hbox addView: sv enablingXResizing: YES];
	[sv release];
	
	{
		GSVbox *vb;
		NSButton *b;
	
		vb=[[GSVbox alloc] init];
		[vb setDefaultMinYMargin: 4];
		[vb setAutoresizingMask: NSViewMinYMargin];
	
		b=[[NSButton alloc] init];
		[b setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
		[b setTitle: _(@"Remove")];
		[b setTarget: self];
		[b setAction: @selector(removeSource:)];
		[b sizeToFit];
		[vb addView: b];
		[b release];
	
		b=[[NSButton alloc] init];
		[b setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
		[b setTitle: _(@"Add")];
		[b setTarget: self];
		[b setAction: @selector(addSource:)];
		[b sizeToFit];
		[vb addView: b];
		[b release];
	
		b=[[NSButton alloc] init];
		[b setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
		[b setTitle: _(@"Properties...")];
		[b setTarget: self];
		[b setAction: @selector(sourceProperties:)];
		[b sizeToFit];
		[vb addView: b];
		[b release];
	
		[hbox addView: vb  enablingXResizing: NO];
	
		[vb release];
	}
	
	[hbox setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];

	top=hbox;

	[[NSNotificationCenter defaultCenter]
		addObserver: self
		selector: @selector(_getSources)
		name: MsgDB_SourceAddNotification
		object: mdb];
	[[NSNotificationCenter defaultCenter]
		addObserver: self
		selector: @selector(_getSources)
		name: MsgDB_SourceRemoveNotification
		object: mdb];
	[[NSNotificationCenter defaultCenter]
		addObserver: self
		selector: @selector(_getSources)
		name: MsgDB_SourceChangeNotification
		object: mdb];

	[self _getSources];
}


- initWithMsgDB: (MsgDB *)m
{
	self=[super init];
	ASSIGN(mdb,m);
	return self;
}

-(void) dealloc
{
	[[NSNotificationCenter defaultCenter]
		removeObserver: self];

	DESTROY(sources);
	DESTROY(mdb);
	DESTROY(source_list);
	DESTROY(c_num);
	DESTROY(c_name);
	DESTROY(c_type);
	DESTROY(top);
	[super dealloc];
}

-(void) setupButton: (NSButton *)b
{
	[b setTitle: _(@"Message\nsources")];
	[b sizeToFit];
}

-(void) willHide
{
}

-(NSView *) willShow
{
	if (!top)
		[self _createViews];
	return top;
}

-(NSString *) name
{
	return _(@"Message sources");
}

-(void) revert
{
	[self _getSources];
}

-(void) save
{
}

@end

