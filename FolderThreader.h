/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef FolderThreader_h
#define FolderThreader_h

#include "MsgDB.h"

typedef struct
{
	msg_id_t mid;
	char rstatus,level;
	unsigned char cached;
} msg_info_t;

@protocol FolderThreaderTarget
-(void) selectMessage: (int)num;
@end

@interface FolderThreader : NSObject
{
@public
	msg_info_t *msgs;
	int num_msgs;
	MsgDB *mdb;

	id<FolderThreaderTarget> t;

	struct msg_header_cache_s *header_cache;
}

- initWithMsgDB: (MsgDB *)mdb  target: (id<FolderThreaderTarget>)target;

-(int) addMsg: (msg_id_t)mid;
-(int) indexOf: (msg_id_t)mid;

-(BOOL) selectFirstUnreadFrom: (int)i;

-(int) findNextUnreadFrom: (int)i;

-(int) findNextMax: (int)max  from: (int)i;
-(int) findPrevMax: (int)max  from: (int)i;

-(void) markAsRead: (int)from : (int)to;


-(NSString *) header: (const char *)h  forMessage: (int)msg;

-(NSString *) subjectForMessage: (int)msg;
-(NSString *) fromForMessage: (int)msg;
-(NSDate *) dateForMessage: (int)msg;

@end


#endif

