/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef FolderListController_h
#define FolderListController_h

#include <AppKit/NSWindowController.h>

@class MsgDB;
@class NSMutableArray,NSTableView,NSTableColumn;

@interface FolderListController : NSWindowController
{
	MsgDB *mdb;
	NSMutableArray *folder_names;

	NSTableView *folders;
	NSTableColumn *c_name,*c_num;
}

- initWithMsgDB: (MsgDB *)m;

@end

#endif

