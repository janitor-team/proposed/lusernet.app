/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef Pref_ReadAhead_h
#define Pref_ReadAhead_h

#include "PrefBox.h"

@class NSButton,NSTextField,GSVbox;

@interface Pref_ReadAhead : NSObject <PrefBox>
{ /* only top is directly retained */
	GSVbox *top;

	NSButton *b_ReadAhead;

	NSButton *b_ReadAheadNextUnread,
		*b_ReadAheadNextThread,
		*b_ReadAheadNext,
		*b_ReadAheadPrevious;

	NSTextField *f_ReadAheadSizeLimit;
}

+(int) readAheadSizeLimit;

+(BOOL) readAhead;
+(BOOL) readAheadNextUnread;
+(BOOL) readAheadNextThread;
+(BOOL) readAheadNext;
+(BOOL) readAheadPrevious;
@end

#endif

