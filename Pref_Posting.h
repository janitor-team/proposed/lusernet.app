/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef Pref_Posting_h
#define Pref_Posting_h

#include "PrefBox.h"


@class GSVbox,NSButton,NSTextField,NSPopUpButton;

@interface Pref_Posting : NSObject <PrefBox>
{
/* only top is directly retained */
	GSVbox *top;

	NSButton *b_PostQuotedPrintable;
	NSTextField *f_PostingName,*f_FromAddress;

	NSTextField *f_SignatureValue;
	NSPopUpButton *b_SignatureType;
}


+(NSString *) postingSignature;


+(NSString *) postingName;
+(NSString *) fromAddress;

+(BOOL) postQuotedPrintable;

@end

#endif

