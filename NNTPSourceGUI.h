/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef NNTPSourceGUI_h
#define NNTPSourceGUI_h

#include "MsgDB.h"
#include "GUISource.h"
#include "NNTPSource.h"

@interface NNTPSource (GUI) <GUISource>
@end


#include "NNTPServer.h"

@interface NNTPSource (private) <NNTPServer_Receiver>
-(void) recreateNNTPServer;
-(void) syncGroupHeaders: (int)gnum;
-(void) syncAllGroupHeaders;
@end

#endif

