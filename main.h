/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef main_h
#define main_h

#include "MsgDB.h"

@class NSApplication,NSMutableDictionary;
@class FolderListController,LogWindowController,PreferencesWindowController;
@class CWMessage;

extern NSString *DefaultEncodingChangedNotification;

@interface AppDelegate : NSObject
{
	NSApplication *app;

	NSMutableDictionary *folder_windows;
	MsgDB *mdb;

	FolderListController *folder_list;
	LogWindowController *log_window;

	PreferencesWindowController *preferences_window;
}
-(void) openFolderWindow: (NSString *)folder_name;
-(void) openMessageWindow: (msg_id_t)mid;

-(void) composeArticle: (NSDictionary *)headers;
-(void) composeFollowupToMessage: (CWMessage *)msg;


-(void) postArticleFrom: (id)sender : (const unsigned char *)data : (int)length; /* TODO */

@end

@interface AppDelegate (prefs)
-(void) openPreferences: (id)sender;
@end
	     

extern AppDelegate *app_delegate;

#endif

